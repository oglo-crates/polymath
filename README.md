# Polymath
A library with stuff for making math in Rust way nicer, and way more flexible!

# Examples:
```rust
// Matrix

use polymath::prelude::*;

fn main() {
  let a: Matrix<f64> = Matrix::new((2, 4), 0.0);
  let b: Matrix<f64> = a + 5.0; // Supports addition. (Of many kinds.)
}
```
