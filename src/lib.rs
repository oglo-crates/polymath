mod types;
mod traits;

#[derive(Debug, Eq, PartialEq, PartialOrd, Ord, Clone, Hash)]
pub struct PolymathError {
    pub error_kind: PolymathErrorKind,
    pub error: String,
}

impl PolymathError {
    /// Create a new Polymath error
    pub fn new(error_kind: PolymathErrorKind, error: &str) -> Self {
        return Self { error_kind, error: error.to_string() };
    }

    /// Same as PolymathError::new(), with the error kind auto set to: Other
    ///
    /// # Examples:
    /// ```rust
    /// use polymath::{PolymathError, PolymathErrorKind};
    ///
    /// fn main() {
    ///     let error1 = PolymathError::new(PolymathErrorKind::Other, "This is an error!");
    ///     let error2 = PolymathError::other("This is an error!");
    ///
    ///     assert!(error1 == error2);
    /// }
    /// ```
    pub fn other(error: &str) -> Self {
        return Self::new(PolymathErrorKind::Other, error);
    }
}

#[derive(Debug, Eq, PartialEq, PartialOrd, Ord, Clone, Hash)]
pub enum PolymathErrorKind {
    Other,
    OutOfBounds,
}

pub mod prelude {
    pub use super::types::*;
    pub use super::traits::*;
}
