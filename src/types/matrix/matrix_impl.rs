#[macro_export]
macro_rules! impl_matrix_number_ops {
    (
        $other_type: ty
    ) => {
        impl Add<$other_type> for Matrix<$other_type> {
            type Output = Matrix<$other_type>;
        
            fn add(self, other: $other_type) -> Self {
                let mut result = self.clone();
        
                result.data = result.data.into_iter()
                    .map(|x| x + other)
                    .collect();
        
                return result;
            }
        }

        impl Sub<$other_type> for Matrix<$other_type> {
            type Output = Matrix<$other_type>;
        
            fn sub(self, other: $other_type) -> Self {
                let mut result = self.clone();
        
                result.data = result.data.into_iter()
                    .map(|x| x - other)
                    .collect();
        
                return result;
            }
        }

        impl Mul<$other_type> for Matrix<$other_type> {
            type Output = Matrix<$other_type>;
        
            fn mul(self, other: $other_type) -> Self {
                let mut result = self.clone();
        
                result.data = result.data.into_iter()
                    .map(|x| x * other)
                    .collect();
        
                return result;
            }
        }

        impl Div<$other_type> for Matrix<$other_type> {
            type Output = Matrix<$other_type>;
        
            fn div(self, other: $other_type) -> Self {
                let mut result = self.clone();
        
                result.data = result.data.into_iter()
                    .map(|x| x / other)
                    .collect();
        
                return result;
            }
        }

        impl Rem<$other_type> for Matrix<$other_type> {
            type Output = Matrix<$other_type>;
        
            fn rem(self, other: $other_type) -> Self {
                let mut result = self.clone();
        
                result.data = result.data.into_iter()
                    .map(|x| x.rem_euclid(other))
                    .collect();
        
                return result;
            }
        }
    };
}

#[macro_export]
macro_rules! impl_matrix_matrix_ops {
    (
        $other_type: ty
    ) => {
        impl Add<Matrix<$other_type>> for Matrix<$other_type> {
            type Output = Matrix<$other_type>;
        
            fn add(self, other: Matrix<$other_type>) -> Self {
                if self.len() != other.len() {
                    panic!("cannot add matrices that are different shapes");
                }

                let mut result = self.clone();

                let length = result.len();

                for i in 0..length {
                    result.data[i] = result.data[i] + other.data[i];
                }

                return result;
            }
        }

        impl Sub<Matrix<$other_type>> for Matrix<$other_type> {
            type Output = Matrix<$other_type>;
        
            fn sub(self, other: Matrix<$other_type>) -> Self {
                if self.len() != other.len() {
                    panic!("cannot subtract matrices that are different shapes");
                }

                let mut result = self.clone();

                let length = result.len();

                for i in 0..length {
                    result.data[i] = result.data[i] - other.data[i];
                }

                return result;
            }
        }

        impl Mul<Matrix<$other_type>> for Matrix<$other_type> {
            type Output = Matrix<$other_type>;
        
            fn mul(self, other: Matrix<$other_type>) -> Self {
                if self.columns != other.rows {
                    panic!("cannot multiply matrices with invalid shapes");
                }

                let mut result = Self::new((self.rows, other.columns), 0 as $other_type);

                for i in 0..self.rows {
                    for j in 0..other.columns {
                        let mut sum = 0 as $other_type;
                        for k in 0..self.columns {
                            let val_a = self.get((i, k)).unwrap();
                            let val_b = other.get((k, j)).unwrap();

                            sum += val_a * val_b;
                        }
                        result.set((i, j), sum).unwrap();
                    }
                }

                return result;
            }
        }
    };
}

#[macro_export]
macro_rules! impl_matrix_type {
    (
        $other_type: ty
    ) => {
        impl_matrix_number_ops!($other_type);
        impl_matrix_matrix_ops!($other_type);

        impl fmt::Debug for Matrix<$other_type> {
            fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
                let mut output = String::new();

                let filler: &str = match f.alternate() {
                    true => "]\n[",
                    false => "], [",
                };

                // Only used with '{:#?}', and not '{:?}'
                let longest_number_length: usize = match f.alternate() {
                    false => 0,
                    true => {
                        let mut length: usize = 0;

                        for i in self.data.iter() {
                            let i_length: usize = format!("{}", i).chars().into_iter().collect::<Vec<char>>().len();

                            if i_length > length {
                                length = i_length;
                            }
                        }

                        length
                    },
                };

                let mut last_row: usize = 1; // So it starts out different on the first run.

                for i in 0..self.len() {
                    let (row, column) = self.delinearize(i);

                    if last_row != row {
                        output.push_str(filler);
                    }

                    if f.alternate() {
                        let mut spacing = String::new();

                        if column != 0 {
                            let i_length = format!("{}", i).chars().into_iter().collect::<Vec<char>>().len();

                            let mut clipped_i_length = i_length;

                            if clipped_i_length > longest_number_length {
                                clipped_i_length = longest_number_length;
                            }

                            for _ in 0..(longest_number_length - (longest_number_length - clipped_i_length) - 1) {
                                spacing.push_str(" ");
                            }
                        }

                        output.push_str(&format!("{spacing}{}", self.get_linear(i).unwrap()));
                    }

                    else {
                        output.push_str(&format!("{}", self.get_linear(i).unwrap()));
                    }

                    if column + 1 != self.columns {
                        output.push_str(", ");
                    }

                    last_row = row;
                }

                output = match f.alternate() {
                    true => format!("{}]", &output[2..]),
                    false => format!("[{}]]", &output[3..]),
                };

                return write!(f, "{output}");
            }
        }
    };
}

pub use impl_matrix_type;
pub use impl_matrix_number_ops;
pub use impl_matrix_matrix_ops;
